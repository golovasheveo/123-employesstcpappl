package telran.employees.api;

import telran.employees.dto.CompanySalary;
import telran.employees.dto.Employee;
import telran.employees.dto.EmployeesReturnCodes;

import java.util.List;
import java.util.Map;

public interface EmployeesService {


    EmployeesReturnCodes addEmployee(Employee empl);

    EmployeesReturnCodes removeEmployee(long id);

    Employee getEmployee(long id);

    /****************************************************/
    Map<String, List<Employee>> getEmployeesGroupedBySalary(int interval);

    /********************************************************/
    Iterable<Employee> getEmployees();

    Iterable<Employee> getEmployeesCompany(String company);

    Iterable<Employee> getEmployeesAges(int ageFrom, int ageTo);

    Iterable<Employee> getEmployeesSalary(int salaryFrom, int salaryTo);

    EmployeesReturnCodes updateCompany(long id, String newCompany);

    EmployeesReturnCodes updateSalary(long id, int newSalary);

    List<CompanySalary> getCompaniesAvgSalary();

    /**
     * @return list of companies with average salary greater than total average salary
     */
    List<CompanySalary> getCompaniesGreaterAvgSalary();

}
