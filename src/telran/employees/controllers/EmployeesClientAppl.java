package telran.employees.controllers;

import telran.employees.api.EmployeesService;
import telran.employees.items.*;
import telran.employees.service.EmployeesTcpProxy;
import telran.menu.ConsoleInputOutput;
import telran.menu.InputOutput;
import telran.menu.Item;
import telran.menu.Menu;


public class EmployeesClientAppl {

    private static final int PORT = 4000;
    private static final String HOST = "localhost";
    private static final InputOutput inputOutput = new ConsoleInputOutput();
    private static final EmployeesService employees = new EmployeesTcpProxy(HOST, PORT);


    public static void main(String[] args) {

        Item[] items = {

                new AddEmployee(employees, inputOutput),
                new RandomGeneration(employees, inputOutput),
                new RemoveEmployees(employees, inputOutput),
                new UpdateCompany(employees, inputOutput),
                new UpdateSalary(employees, inputOutput),
                new DisplayEmployee(employees, inputOutput),
                new DisplayAllEmployees(employees, inputOutput),
                new DisplayEmployeesSalary(employees, inputOutput),
                new DisplayEmployeesAges(employees, inputOutput),
                new DisplayEmployeesCompany(employees, inputOutput),

                new DisplayCompaniesAvgSalary(employees, inputOutput),
                new DisplayCompaniesGreaterAvgSalary(employees, inputOutput),

                new ExitEmployeesItem(employees, inputOutput),

        };

        Menu menu = new Menu(items, inputOutput);
        menu.menuRun();
    }
}
