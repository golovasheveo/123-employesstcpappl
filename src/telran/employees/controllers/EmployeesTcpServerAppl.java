package telran.employees.controllers;

import telran.employees.api.EmployeesService;
import telran.employees.service.EmployeesServiceMapsImpl;
import telran.employees.service.EmployeesTcpProtocol;
import telran.net.server.ProtocolJava;
import telran.net.server.ServerJava;

public class EmployeesTcpServerAppl {

    private static final int PORT = 4000;


    public static void main(String[] args) {

        EmployeesService employees = new EmployeesServiceMapsImpl();
        ProtocolJava protocol = new EmployeesTcpProtocol(employees);
        ServerJava server = new ServerJava(protocol, PORT);
        server.run();
    }

}