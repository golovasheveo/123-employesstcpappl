package telran.employees.service;

import telran.employees.api.EmployeesService;
import telran.employees.dto.CompanySalary;
import telran.employees.dto.Employee;
import telran.employees.dto.EmployeesReturnCodes;
import telran.employees.dto.RequestObjects;
import telran.net.RequestJava;
import telran.net.ResponseJava;
import telran.net.TcpResponseCode;
import telran.net.server.ProtocolJava;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("ALL")
public class EmployeesTcpProtocol implements ProtocolJava {

    private final EmployeesService employees;
    String ADD_EMPLOYEE = "addEmployee";
    String REMOVE_EMPLOYEE = "removeEmployee";
    String GET_EMPLOYEE = "getEmployee";
    String GET_EMPLOYEES = "getEmployees";
    String GET_EMPLOYEES_AGES = "getEmployeesAges";
    String GET_EMPLOYEES_COMPANY = "getEmployeesCompany";
    String GET_EMPLOYEES_SALARY = "getEmployeesSalary";
    String UPDATE_COMPANY = "updateCompany";
    String UPDATE_SALARY = "updateSalary";
    String GET_EMPLOYEES_GROUPER_BY_SALARY = "getEmployeesGroupedBySalary";
    String GET_COMPANIES_AVG_SALARY = "getCompaniesAvgSalary";
    String GET_COMPANIES_GREATER_AVG_SALARY = "getCompaniesGreaterAvgSalary";
    private HashMap<String, Function<Serializable, ResponseJava>> mapFunctions;


    public EmployeesTcpProtocol(EmployeesService employees) {
        this.employees = employees;
        fillMapFunctions();
    }

    private void fillMapFunctions() {

        mapFunctions = new HashMap<>();
        mapFunctions.put(ADD_EMPLOYEE, this::addEmployee);
        mapFunctions.put(REMOVE_EMPLOYEE, this::removeEmployee);
        mapFunctions.put(GET_EMPLOYEE, this::getEmployee);
        mapFunctions.put(GET_EMPLOYEES, this::getEmployees);
        mapFunctions.put(GET_EMPLOYEES_AGES, this::getEmployeesAges);
        mapFunctions.put(GET_EMPLOYEES_COMPANY, this::getEmployeesCompany);
        mapFunctions.put(GET_EMPLOYEES_SALARY, this::getEmployeesSalary);
        mapFunctions.put(UPDATE_COMPANY, this::updateCompany);
        mapFunctions.put(UPDATE_SALARY, this::updateSalary);
        mapFunctions.put(GET_EMPLOYEES_GROUPER_BY_SALARY, this::getEmployeesGroupedBySalary);
        mapFunctions.put(GET_COMPANIES_AVG_SALARY, this::getCompaniesAvgSalary);
        mapFunctions.put(GET_COMPANIES_GREATER_AVG_SALARY, this::getCompaniesGreaterAvgSalary);
    }


    @Override
    public ResponseJava getResponse(RequestJava request) {
        Function<Serializable, ResponseJava> function =
                mapFunctions.getOrDefault(request.requestType, this::wrongRequest);
        return function.apply(request.requestData);
    }


    private ResponseJava addEmployee(Serializable RequestObjects) {
        try {
            EmployeesReturnCodes res = employees.addEmployee((Employee) RequestObjects);
            return new ResponseJava(TcpResponseCode.OK, res);
        } catch (Exception e) {
            return new ResponseJava(TcpResponseCode.UNKNOWN, e.getMessage());
        }
    }


    private ResponseJava removeEmployee(Serializable RequestObjects) {
        try {
            EmployeesReturnCodes res = employees.removeEmployee((long) RequestObjects);
            return new ResponseJava(TcpResponseCode.OK, res);
        } catch (Exception e) {
            return new ResponseJava(TcpResponseCode.UNKNOWN, e.getMessage());
        }
    }


    private ResponseJava getEmployee(Serializable RequestObjects) {

        try {
            Employee res = employees.getEmployee((long) RequestObjects);
            return new ResponseJava(TcpResponseCode.OK, (Serializable) res);
        } catch (Exception e) {
            return new ResponseJava(TcpResponseCode.UNKNOWN, e.getMessage());
        }
    }


    private ResponseJava getEmployees(Serializable RequestObjects) {

        try {
            Iterable<Employee> res = employees.getEmployees();
            return new ResponseJava(TcpResponseCode.OK, (Serializable) res);
        } catch (Exception e) {
            return new ResponseJava(TcpResponseCode.UNKNOWN, e.getMessage());
        }
    }

    private ResponseJava getEmployeesAges(Serializable RequestObjects) {
        try {
            RequestObjects data = (RequestObjects) RequestObjects;
            Iterable<Employee> res = employees.getEmployeesAges((int) data.firstObject, (int) data.secondObject);
            return new ResponseJava(TcpResponseCode.OK, (Serializable) res);
        } catch (Exception e) {
            return new ResponseJava(TcpResponseCode.UNKNOWN, e.getMessage());
        }
    }


    private ResponseJava getEmployeesCompany(Serializable RequestObjects) {
        try {
            Iterable<Employee> res = employees.getEmployeesCompany((String) RequestObjects);
            return new ResponseJava(TcpResponseCode.OK, (Serializable) res);
        } catch (Exception e) {
            return new ResponseJava(TcpResponseCode.UNKNOWN, e.getMessage());
        }
    }


    private ResponseJava updateSalary(Serializable RequestObjects) {
        try {
            RequestObjects data = (RequestObjects) RequestObjects;
            EmployeesReturnCodes res = employees.updateSalary((long) data.firstObject, (int) data.secondObject);
            return new ResponseJava(TcpResponseCode.OK, res);
        } catch (Exception e) {
            return new ResponseJava(TcpResponseCode.UNKNOWN, e.getMessage());
        }
    }

    private ResponseJava updateCompany(Serializable RequestObjects) {
        try {
            RequestObjects data = (RequestObjects) RequestObjects;
            EmployeesReturnCodes res = employees.updateCompany((long) data.firstObject, (String) data.secondObject);
            return new ResponseJava(TcpResponseCode.OK, res);
        } catch (Exception e) {
            return new ResponseJava(TcpResponseCode.UNKNOWN, e.getMessage());
        }
    }

    private ResponseJava getEmployeesSalary(Serializable RequestObjects) {
        try {
            RequestObjects data = (RequestObjects) RequestObjects;
            Iterable<Employee> res = employees.getEmployeesSalary((int) data.firstObject, (int) data.secondObject);
            return new ResponseJava(TcpResponseCode.OK, (Serializable) res);
        } catch (Exception e) {
            return new ResponseJava(TcpResponseCode.UNKNOWN, e.getMessage());
        }
    }

    private ResponseJava getEmployeesGroupedBySalary(Serializable RequestObjects) {
        try {
            Map<String, List<Employee>> res = employees.getEmployeesGroupedBySalary((Integer) RequestObjects);
            return new ResponseJava(TcpResponseCode.OK, (Serializable) res);
        } catch (Exception e) {
            return new ResponseJava(TcpResponseCode.UNKNOWN, e.getMessage());
        }
    }

    private ResponseJava getCompaniesAvgSalary(Serializable serializable) {
        try {
            List<CompanySalary> res = employees.getCompaniesAvgSalary();
            return new ResponseJava(TcpResponseCode.OK, (Serializable) res);
        } catch (Exception e) {
            return new ResponseJava(TcpResponseCode.UNKNOWN, e.getMessage());
        }
    }

    private ResponseJava getCompaniesGreaterAvgSalary(Serializable serializable) {

        try {
            List<CompanySalary> res = employees.getCompaniesGreaterAvgSalary();
            return new ResponseJava(TcpResponseCode.OK, (Serializable) res);
        } catch (Exception e) {
            return new ResponseJava(TcpResponseCode.UNKNOWN, e.getMessage());
        }
    }

    private ResponseJava wrongRequest(Serializable RequestObjects) {
        return new ResponseJava(TcpResponseCode.WRONG_REQUEST, "Request not found");
    }
}