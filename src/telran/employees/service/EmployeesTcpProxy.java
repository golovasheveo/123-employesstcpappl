package telran.employees.service;

import telran.employees.api.EmployeesService;
import telran.employees.dto.CompanySalary;
import telran.employees.dto.Employee;
import telran.employees.dto.EmployeesReturnCodes;
import telran.employees.dto.RequestObjects;
import telran.net.TcpClientJava;

import java.util.List;
import java.util.Map;


public class EmployeesTcpProxy extends TcpClientJava implements EmployeesService {

    String ADD_EMPLOYEE = "addEmployee";
    String REMOVE_EMPLOYEE = "removeEmployee";
    String GET_EMPLOYEE = "getEmployee";
    String GET_EMPLOYEES = "getEmployees";
    String GET_EMPLOYEES_AGES = "getEmployeesAges";
    String GET_EMPLOYEES_COMPANY = "getEmployeesCompany";
    String GET_EMPLOYEES_SALARY = "getEmployeesSalary";
    String UPDATE_COMPANY = "updateCompany";
    String UPDATE_SALARY = "updateSalary";
    String GET_EMPLOYEES_GROUPER_BY_SALARY = "getEmployeesGroupedBySalary";
    String GET_COMPANIES_AVG_SALARY = "getCompaniesAvgSalary";
    String GET_COMPANIES_GREATER_AVG_SALARY = "getCompaniesGreaterAvgSalary";

    public EmployeesTcpProxy(String hostname, int port) {
        super(hostname, port);
    }

    @Override
    public EmployeesReturnCodes addEmployee(Employee empl) {
        return sendRequest(ADD_EMPLOYEE, empl);
    }

    @Override
    public EmployeesReturnCodes removeEmployee(long id) {
        return sendRequest(REMOVE_EMPLOYEE, id);
    }

    @Override
    public Employee getEmployee(long id) {
        return sendRequest(GET_EMPLOYEE, id);
    }

    @Override
    public Iterable<Employee> getEmployees() {
        return sendRequest(GET_EMPLOYEES, null);
    }

    @Override
    public Iterable<Employee> getEmployeesCompany(String company) {
        return sendRequest(GET_EMPLOYEES_COMPANY, company);
    }

    @Override
    public Iterable<Employee> getEmployeesAges(int ageFrom, int ageTo) {
        RequestObjects data = new RequestObjects(ageFrom, ageTo);
        return sendRequest(GET_EMPLOYEES_AGES, data);
    }

    @Override
    public Iterable<Employee> getEmployeesSalary(int salaryFrom, int salaryTo) {
        RequestObjects data = new RequestObjects(salaryFrom, salaryTo);
        return sendRequest(GET_EMPLOYEES_SALARY, data);
    }

    @Override
    public EmployeesReturnCodes updateCompany(long id, String newCompany) {
        RequestObjects data = new RequestObjects(id, newCompany);
        return sendRequest(UPDATE_COMPANY, data);

    }

    @Override
    public EmployeesReturnCodes updateSalary(long id, int newSalary) {
        RequestObjects data = new RequestObjects(id, newSalary);
        return sendRequest(UPDATE_SALARY, data);
    }

    @Override
    public Map<String, List<Employee>> getEmployeesGroupedBySalary(int interval) {
        return sendRequest(GET_EMPLOYEES_GROUPER_BY_SALARY, interval);
    }

    @Override
    public List<CompanySalary> getCompaniesAvgSalary() {
        return sendRequest(GET_COMPANIES_AVG_SALARY, null);
    }

    @Override
    public List<CompanySalary> getCompaniesGreaterAvgSalary() {
        return sendRequest(GET_COMPANIES_GREATER_AVG_SALARY, null);
    }
}