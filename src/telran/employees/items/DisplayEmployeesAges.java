package telran.employees.items;

import telran.employees.api.EmployeesService;
import telran.menu.InputOutput;

public class DisplayEmployeesAges extends EmployeesItem {


    public DisplayEmployeesAges(EmployeesService employees, InputOutput inputOutput) {

        super(employees, inputOutput);

    }


    @Override

    public String displayName() {
        return "Display all employees with ages ";
    }


    @Override
    public void perform() {
        int min = inputOutput.inputInteger("Enter min age");
        int max = inputOutput.inputInteger("Enter max to", min, Integer.MAX_VALUE);
        inputOutput.displayLine(employees.getEmployeesAges(min, max));
    }

}