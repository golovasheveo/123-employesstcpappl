package telran.employees.items;

import telran.employees.api.EmployeesService;
import telran.menu.InputOutput;

public class DisplayCompaniesAvgSalary extends EmployeesItem {


    public DisplayCompaniesAvgSalary(EmployeesService employees, InputOutput inputOutput) {
        super(employees, inputOutput);
    }


    @Override
    public String displayName() {
        return "Display companies average salary";
    }


    @Override
    public void perform() {
        inputOutput.displayLine(employees.getCompaniesAvgSalary());
    }

}