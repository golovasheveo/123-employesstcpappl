package telran.employees.items;

import telran.employees.api.EmployeesService;
import telran.menu.InputOutput;

public class DisplayEmployeesCompany extends EmployeesItem {


    public DisplayEmployeesCompany(EmployeesService employees, InputOutput inputOutput) {
        super(employees, inputOutput);
    }


    @Override
    public String displayName() {
        return "Display of employees by company name";
    }


    @Override

    public void perform() {
        String company = inputOutput.inputString("Enter company name");
        inputOutput.displayLine(employees.getEmployeesCompany(company));
    }

}