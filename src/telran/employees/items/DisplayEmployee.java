package telran.employees.items;

import telran.employees.api.EmployeesService;
import telran.menu.InputOutput;

public class DisplayEmployee extends EmployeesItem {
    public DisplayEmployee(EmployeesService employees, InputOutput inputOutput) {

        super(employees, inputOutput);
    }


    @Override
    public String displayName() {
        return "Get employee by id";
    }


    @Override
    public void perform() {
        long id = inputOutput.inputInteger("Enter employee id");
        inputOutput.displayLine(employees.getEmployee(id));
    }

}