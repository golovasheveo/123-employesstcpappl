package telran.employees.items;

import telran.employees.api.EmployeesService;
import telran.menu.InputOutput;

public class DisplayAllEmployees extends EmployeesItem {


    public DisplayAllEmployees(EmployeesService employees, InputOutput inputOutput) {
        super(employees, inputOutput);
    }


    @Override
    public String displayName() {
        return "Display all employees";
    }

    @Override
    public void perform() {
        inputOutput.displayLine(employees.getEmployees());
    }

}