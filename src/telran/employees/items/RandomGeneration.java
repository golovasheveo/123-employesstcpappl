package telran.employees.items;

import telran.employees.api.EmployeesService;
import telran.employees.dto.Employee;
import telran.employees.dto.EmployeesReturnCodes;
import telran.menu.InputOutput;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomGeneration extends EmployeesItem {
    private static int id = 100;
    private static int minSalary;
    private static int maxSalary;
    private static int nCompanies;
    private static int nEmployees;
    private static LocalDate minYear;
    private static LocalDate maxYear;

    public RandomGeneration(EmployeesService employees, InputOutput inputOutput) {
        super(employees, inputOutput);
    }

    private static Employee getRandomEmployee() {
        LocalDate birthDate = getRandomBirthDay(minYear, maxYear);
        String company = "Company" + getRandomNumber(1, nCompanies);
        String name = "Name" + getRandomNumber(1, nEmployees);
        int salary = getRandomNumber(minSalary, maxSalary);
        return new Employee(id++, salary, company, birthDate, name);
    }

    private static int getRandomNumber(int min, int max) {
        return (int) (min + Math.random() * (max - min));
    }
    

    private static LocalDate getRandomBirthDay(LocalDate min, LocalDate max) {
        long random = ThreadLocalRandom
                .current()
                .nextLong(min.toEpochDay(), max.toEpochDay());
        return LocalDate.ofEpochDay(random);
    }

    @Override
    public String displayName() {
        return "Generation employees";
    }

    @Override
    public void perform() {
        nEmployees = inputOutput.inputInteger("Enter the number of employees");
        nCompanies = inputOutput.inputInteger("Enter the number of companies");
        minSalary = inputOutput.inputInteger("Enter MIN salary");
        maxSalary = inputOutput.inputInteger("Enter MAX salary");
        minYear = inputOutput.inputDate("Enter MIN birth date YYYY-MM-DD");
        maxYear = inputOutput.inputDate("Enter MAX birth date YYYY-MM-DD");
        getRandomPersons();
    }

    private void getRandomPersons() {
        HashMap<EmployeesReturnCodes, Integer> res = Stream.generate(RandomGeneration::getRandomEmployee)
                .distinct()
                .limit(nEmployees).map(e -> employees.addEmployee(e))
                .collect(Collectors.toMap(emp -> emp, emp -> 1, Integer::sum, HashMap::new));
        inputOutput.displayLine(res);
    }
}
