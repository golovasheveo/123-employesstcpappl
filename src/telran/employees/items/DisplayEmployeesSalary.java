package telran.employees.items;

import telran.employees.api.EmployeesService;
import telran.menu.InputOutput;

public class DisplayEmployeesSalary extends EmployeesItem {

    public DisplayEmployeesSalary(EmployeesService employees, InputOutput inputOut) {
        super(employees, inputOut);

    }

    @Override
    public String displayName() {
        return "Display all employees with salary";
    }

    @Override
    public void perform() {
        int min = inputOutput.inputInteger("Enter min salary");
        int max = inputOutput.inputInteger("Enter max salary", min, Integer.MAX_VALUE);
        inputOutput.displayLine(employees.getEmployeesSalary(min, max));
    }

}
