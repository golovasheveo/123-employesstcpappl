package telran.employees.items;

import telran.employees.api.EmployeesService;
import telran.menu.InputOutput;

public class DisplayCompaniesGreaterAvgSalary extends EmployeesItem {


    public DisplayCompaniesGreaterAvgSalary(EmployeesService employees, InputOutput inputOutput) {
        super(employees, inputOutput);
    }


    @Override

    public String displayName() {
        return "Display companies greater average salary";
    }


    @Override

    public void perform() {
        inputOutput.displayLine(employees.getCompaniesGreaterAvgSalary());
    }
}