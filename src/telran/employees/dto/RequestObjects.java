package telran.employees.dto;

import java.io.Serializable;

public class RequestObjects implements Serializable {

    public Object firstObject;
    public Object secondObject;

    public RequestObjects(Object firstObject, Object secondObject) {
        this.firstObject = firstObject;
        this.secondObject = secondObject;
    }

}